# muycode-base-web

> 管理后台
搭配 [muycode-base](https://gitee.com/muycode/maven) 后端jar使用

## Build Setup

```bash
# 克隆项目
git clone https://gitee.com/muycode/muycode-base-web.git

# 进入项目目录
cd muycode-base-web

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 [http://localhost:9528](http://localhost:9528)

# 引入 组件
```bash
# 引入 EChatrs
npm install echarts --save 
npm install -g stylelint
# 修改请求参数方式
npm install qs --save-dev
# 图片剪切
npm install vue-cropper --save

# 富文本 https://www.wangeditor.com/doc/
npm i wangeditor --save

# 全屏
npm install vue-fullscreen

# 升级 element-ui
npm uninstall element-ui
npm i element-ui -S   
npm i element-ui@2.15.7 -s

# 抽奖
npm install @lucky-canvas/vue

# 粒子
npm install vue-particles --save-dev

# md5
npm install --save js-md5

```

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 其它

```bash
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```
     
## 出现错误

如果日期组件报错，则再重新执行一下``npm i element-ui@2.15.7 -s``

## 效果

![img](https://all-imgs.muycode.com/iTools/202206091042305468.png?imageMogr2/format/webp/blur/1x0/quality/75|watermark/2/text/bXV5Y29kZQ==/font/5qW35L2T/fontsize/280/fill/Izg1ODI4Mg==/dissolve/80/gravity/SouthEast/dx/10/dy/10)
