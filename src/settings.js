module.exports = {

  title: '平台后台管理',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: false,
  /**
   * 标记请求状态，如果出现异常，则只需要提醒一次
   */
  canRequest: true,
  /**
   * 未读消息
   */
  noticeTotal: 0,
  /**
   * @description api请求基础路径
   */
  baseUrl: {
    dev: 'http://127.0.0.1:8751/',
    pro: ''
  }
}
