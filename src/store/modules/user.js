import { buildMenu, login, logout } from '@/api/user'
import {
  getToken,
  getUser,
  removeToken,
  setAccess,
  setOrgName,
  setRefreshToken,
  setToken,
  setUser,
  setUsername,
  tokenClear
} from '@/utils/auth'
import router, { resetRouter } from '@/router'
import Layout from '@/layout'
import { errorMessage } from '@/utils/alertMessage'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: ''
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROUTER: (state, routers) => {
    state.routers = routers
  }
}

// 设置路由
const addRouter = ({ commit }, menusArr) => {
  // 声明一个空的数组，储存菜单对应的路由
  const routerArrList = []
  if (menusArr == null || !menusArr) {
    return
  }
  menusArr.forEach((menu) => {
    const outMenu = {
      path: '/',
      component: Layout,
      children: []
    }
    if (menu.children && menu.children.length > 0) {
      outMenu.id = menu['rid']
      outMenu.meta = { title: menu['title'], icon: menu['icon'] }
      getChildRoute(menu.children, outMenu)
    } else {
      const addr = menu['component']
      if (addr) {
        const menuChild = {
          id: menu['rid'],
          path: menu['path'],
          name: menu['path'].replaceAll('/', ''),
          component: () => Promise.resolve().then(() => require(`@/views${addr}`)),
          meta: { title: menu['title'], icon: menu['icon'] }
        }
        if (menu['title'] === '首页') {
          outMenu.redirect = '/home'
        }
        outMenu.children.push(menuChild)
      }
    }
    routerArrList.push(outMenu)
  })
  commit('SET_ROUTER', routerArrList)
  router.addRoutes(routerArrList)
}

// 多级菜单
const getChildRoute = (childArr, outMenu) => {
  childArr.forEach((item) => {
    if (item.children && item.children.length > 0) {
      getChildRoute(item.children, outMenu)
    } else {
      const addr = item['component']
      if (addr) {
        const menuChild = {
          id: item['rid'],
          path: item['path'],
          name: item['path'].replaceAll('/', ''),
          component: () => Promise.resolve().then(() => require(`@/views${addr}`)),
          meta: { title: item['title'], icon: item['icon'] }
        }
        outMenu.children.push(menuChild)
      }
    }
  })
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password, code, key } = userInfo
    return new Promise((resolve, reject) => {
      login({ loginName: username.trim(), password: password, code: code, key: key }).then(response => {
        const { data } = response.data
        if (data && response.status === 200) {
          commit('SET_TOKEN', data['token'])
          setToken(data['token'])
          setRefreshToken(data['refreshToken'])
          if (!data['avatar']) {
            // data.user.avatar = 'https://all-imgs.muycode.com/heart-1.jpg'
            // data['avatar'] = 'https://life.muycode.com/DSC_1566.jpg'
            data['avatar'] = 'https://all-imgs.muycode.com/m-auth.jpg'
          }
          setUsername(data['loginName'])
          // orgName
          setOrgName(data['orgName'])
          setAccess(data['mids'])
          data['mids'] = ''
          setUser(JSON.stringify(data))
          resolve('success')
        } else {
          errorMessage(response.data.message)
          resolve('error')
        }
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      const user = JSON.parse(getUser())
      commit('SET_NAME', user['loginName'])
      commit('SET_AVATAR', user['avatar'])
      resolve(user)
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        tokenClear()
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  },

  // 获取路由
  getRoutrPath({ commit }) {
    return new Promise((resolve, reject) => {
      buildMenu().then(res => {
        const { data } = res.data
        addRouter({ commit }, data)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

