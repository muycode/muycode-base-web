const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  routers: state => state.user.routers,
  name: state => state.user.name
}
export default getters
