import ajax from '@/utils/ajaxApi'
import { errorMessage } from '@/utils/alertMessage'

const noticeAction = {}

// 查询消息
noticeAction.queryAllListByUser = function(vm) {
  ajax.get('systemNotice/queryAllListByUser', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data.list
        vm.tableData.total = res.data.total
        vm.tableData.page = res.data.pageNum
        vm.tableData.size = res.data.pageSize
        vm.listLoading = false
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      errorMessage(res.message)
    }
  )
}

noticeAction.readNotice = function(vm) {
  ajax.post('systemNotice/readNotice', vm.readForm,
    res => {
      if (res.code !== 200) {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default noticeAction
