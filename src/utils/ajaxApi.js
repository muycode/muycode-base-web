import axios from 'axios'
import settings from '@/settings'
import router from '@/router'
import {
  getRefreshToken,
  getToken,
  setAccess,
  setOrgName,
  setRefreshToken,
  setToken,
  setUser,
  setUsername,
  tokenClear
} from '@/utils/auth'
import { refreshToken } from '@/api/user'
import { errorMessage, notifyWarning } from '@/utils/alertMessage'

let cancel
const promiseArr = {}

// 请求超时的时间限制
axios.defaults.timeout = 60000

const baseUrl = process.env.NODE_ENV === 'development' ? settings.baseUrl.dev : settings.baseUrl.pro

// 请求拦截器
axios.interceptors.request.use(config => {
  const token = getToken()
  const refreshToken = getRefreshToken()
  if (token && refreshToken) {
    config.headers = {
      'Content-Type': 'application/json;charset=UTF-8',
      'Authorization': 'Bearer ' + getToken(),
      'refresh_token': getRefreshToken()
    }
  } else {
    config.headers = {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  }

  if (promiseArr[config.url]) {
    promiseArr[config.url]('操作取消')
    promiseArr[config.url] = cancel
  } else {
    promiseArr[config.url] = cancel
  }
  return config
}, error => {
  return Promise.reject(error)
})

// 响应拦截器异常处理
axios.interceptors.response.use(response => {
  settings.canRequest = true
  return response
}, (error) => {
  if (!settings.canRequest) {
    if (error.response.status === 515) {
      router.push('/system-maintenance')
    }
    return false
  }
  settings.canRequest = false
  if (error && error.response) {
    switch (error.response.status) {
      case 515:
        // 系统维护中...
        router.push('/system-maintenance')
        break
      case 400:
        error.message = '错误请求'
        break
      case 401:
        tokenClear()
        router.push('/login')
        error.message = '未授权，请重新登录'
        break
      case 402:
        error.message = '无效 Token 凭证'
        break
      case 403:
        tokenClear()
        router.push('/login')
        error.message = '账户异地登录'
        break
      case 404:
        error.message = '请求错误,未找到该资源'
        break
      case 405:
        // 无权限访问
        settings.canRequest = true
        error.message = error.response.data.message
        break
      case 411:
        tokenClear()
        router.push('/login')
        error.message = '您已被加入黑名单列表，请联系管理员处理！'
        break
      case 413:
        tokenClear()
        router.push('/login')
        error.message = '检测到IP地址变化，请重新登录'
        break
      case 408:
        error.message = '请求超时'
        break
      case 410:
        error.message = '需要重新刷新 Token 凭证！'
        break
      case 500:
        error.message = '服务器异常，请稍候再试'
        break
      case 501:
        error.message = '网络未实现'
        break
      case 502:
        error.message = '网络错误'
        break
      case 503:
        error.message = '服务不可用'
        break
      case 504:
        error.message = '网络超时'
        break
      case 505:
        error.message = 'http版本不支持该请求'
        break
      case 516:
        error.message = '非法参数'
        break
      default:
        error.message = `连接错误${error.response.status}`
    }
  } else {
    error.message = '连接到服务器失败'
    errorMessage(error.message)
  }

  if (error.response.status === 412) {
    notifyWarning('由于您长时间未操作，已为您刷新 Token 凭证！\n 即将为您刷新页面！')
    // 刷新token凭证
    setTimeout(() => {
      refreshToken().then(res => {
        const { data } = res.data
        if (data && res.status === 200) {
          setToken(data['token'])
          setRefreshToken(data['refreshToken'])
          if (!data['avatar']) {
            // data.user.avatar = 'https://all-imgs.muycode.com/heart-1.jpg'
            // data['avatar'] = 'https://life.muycode.com/DSC_1566.jpg'
            data['avatar'] = 'https://all-imgs.muycode.com/m-auth.jpg'
          }
          setUsername(data['loginName'])
          // orgName
          setOrgName(data['orgName'])
          setAccess(data['mids'])
          data['mids'] = ''
          setUser(JSON.stringify(data))
          setTimeout(function() {
            router.go(0)
          }, 100)
        } else {
          tokenClear()
          router.push('/login')
          errorMessage('登录失败')
        }
      })
    }, 100)
  } else {
    if (error.response.status !== 515) {
      errorMessage(error.message)
    }
  }
  return Promise.resolve(error.response)
})

// 自定义判断元素类型JS
function toType(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}

// 参数过滤函数
function filterNull(o) {
  for (const key in o) {
    if (o[key] === null) {
      delete o[key]
    }
    if (toType(o[key]) === 'string') {
      o[key] = o[key].trim()
    } else if (toType(o[key]) === 'object') {
      o[key] = filterNull(o[key])
    } else if (toType(o[key]) === 'array') {
      o[key] = filterNull(o[key])
    }
  }
  return o
}

/*
 * 接口处理函数
 */
function apiAxios(method, url, params, success, failure) {
  // settings.canRequest = true
  if (params) {
    params = filterNull(params)
  }

  url = baseUrl + url

  axios({
    method: method,
    url: url,
    data: method === 'POST' || method === 'PUT' ? params : null,
    params: method === 'GET' || method === 'DELETE' ? params : null,
    withCredentials: false
  }).then(function(res) {
    if (res.status === 200) {
      if (success) {
        success(res.data)
      }
    } else {
      if (failure) {
        failure(res)
      }
    }
  }).catch(function(err) {
    const res = err.response
    if (res) {
      failure(err.response)
      errorMessage('api error, HTTP CODE: ' + res.status)
    }
  })
}

/*
 * 接口处理函数
 */
function apiAxiosFile(url, param, config, success, failure) {
  axios.post(
    url, param, config
  )
    .then(function(res) {
      if (res.status === 200) {
        if (success) {
          success(res.data)
        }
      } else {
        if (failure) {
          failure(res)
        }
      }
    }).catch(function(err) {
      const res = err.response
      if (res) {
        failure(err.response)
        errorMessage('api error, HTTP CODE: ' + res.status)
      }
    })
}

// 返回在vue模板中的调用接口
export default {
  get: function(url, params, success, failure) {
    return apiAxios('GET', url, params, success, failure)
  },
  post: function(url, params, success, failure) {
    return apiAxios('POST', url, params, success, failure)
  },
  postFile: function(url, params, config, success, failure) {
    return apiAxiosFile(url, params, config, success, failure)
  },
  put: function(url, params, success, failure) {
    return apiAxios('PUT', url, params, success, failure)
  },
  delete: function(url, params, success, failure) {
    return apiAxios('DELETE', url, params, success, failure)
  }
}
