import { decode, encrypt } from '@/api/encryptUtil'

const Authorization = 'authorization'
const RefreshToken = 'refresh_token'
const Username = 'username'
const user = 'user'
const access = 'access'
const OrgName = 'orgName'

export function getToken() {
  return localStorage.getItem(Authorization)
}

export function setToken(token) {
  return localStorage.setItem(Authorization, token)
}

export function removeToken() {
  return localStorage.removeItem(Authorization)
}

export function tokenClear() {
  return localStorage.clear()
}

export function getUsername() {
  return localStorage.getItem(Username)
}

export function setUsername(username) {
  return localStorage.setItem(Username, username)
}

export function setOrgName(orgName) {
  return localStorage.setItem(OrgName, orgName)
}

export function getRefreshToken() {
  return localStorage.getItem(RefreshToken)
}

export function setRefreshToken(refreshToken) {
  return localStorage.setItem(RefreshToken, refreshToken)
}

export function removeRefreshToken() {
  return localStorage.removeItem(RefreshToken)
}

export function setUser(userdata) {
  return localStorage.setItem(user, userdata)
}

export function getUser() {
  return localStorage.getItem(user)
}

export function getAccess() {
  return decode(localStorage.getItem(access))
}

export function setAccess(data) {
  localStorage.setItem(access, encrypt(JSON.stringify(data)))
}

export function checkAccess(auth) {
  return JSON.parse(decode(localStorage.getItem(access))).includes(auth)
}
