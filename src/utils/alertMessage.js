import { Message, Notification } from 'element-ui'

// 对外使用消息，可随时切换消息主题

// 成功消息
export function successMessage(message) {
  notifySuccess(message)
  // messageSuccess(message)
}

// 错误消息
export function errorMessage(message) {
  notifyError(message)
  // messageError(message)
}

// 警告消息
export function warningMessage(message) {
  notifyWarning(message)
  // messageWarning(message)
}

// 提示消息
export function infoMessage(message) {
  notifyInfo(message)
  // messageInfo(message)
}

/* 以下是方法 */

// 成功消息
export function notifySuccess(message) {
  if (!message || message.length === 0) {
    message = '操作成功'
  }
  Notification.success({
    title: '成功消息',
    message: message
  })
}

// 错误消息
export function notifyError(message) {
  if (message.length > 0) {
    Notification.error({
      title: '错误消息',
      message: message
    })
  }
}

// 警告消息
export function notifyWarning(message) {
  if (message.length > 0) {
    Notification.warning({
      title: '警告消息',
      message: message
    })
  }
}

// 提示消息
export function notifyInfo(message) {
  if (message.length > 0) {
    Notification.info({
      title: '提示消息',
      message: message
    })
  }
}

// 成功消息
export function messageSuccess(message) {
  if (!message || message.length === 0) {
    message = '操作成功'
  }
  Message.success({
    message: message,
    center: true
  })
}

// 错误消息
export function messageError(message) {
  if (message.length > 0) {
    Message.error({
      message: message,
      center: true
    })
  }
}

// 警告消息
export function messageWarning(message) {
  if (message.length > 0) {
    Message.warning({
      message: message,
      center: true
    })
  }
}

// 提示消息
export function messageInfo(message) {
  if (message.length > 0) {
    Message.info({
      message: message,
      center: true
    })
  }
}
