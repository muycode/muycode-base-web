import axios from 'axios'
import {
  getRefreshToken,
  getToken,
  setAccess,
  setOrgName,
  setRefreshToken,
  setToken,
  setUser,
  setUsername,
  tokenClear
} from '@/utils/auth'
import router from '@/router'
import settings from '@/settings'
import { refreshToken } from '@/api/user'
import { errorMessage, notifyWarning } from '@/utils/alertMessage'

// create an axios instance
const service = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? settings.baseUrl.dev : settings.baseUrl.pro,
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 60000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    const token = getToken()
    const refreshToken = getRefreshToken()

    if (token && refreshToken) {
      config.headers = {
        'Content-Type': 'application/json',
        'withCredentials': 'true',
        'Authorization': 'Bearer ' + getToken(),
        'refresh_token': getRefreshToken()
      }
    } else {
      config.headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json'
      }
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    return response
  },
  error => {
    if (error && error.response) {
      switch (error.response.status) {
        case 515:
          // 系统维护中...
          router.push('/system-maintenance')
          break
        case 400:
          error.message = '错误请求'
          break
        case 401:
          tokenClear()
          router.push('/login')
          error.message = '未授权，请重新登录'
          break
        case 402:
          error.message = '无效 Token 凭证'
          break
        case 403:
          tokenClear()
          router.push('/login')
          error.message = '账户异地登录'
          break
        case 404:
          error.message = '请求错误,未找到该资源'
          break
        case 405:
          error.message = error.response.data.message
          break
        case 411:
          tokenClear()
          router.push('/login')
          error.message = '您已被加入黑名单列表，请联系管理员处理！'
          break
        case 413:
          tokenClear()
          router.push('/login')
          error.message = '检测到IP地址变化，请重新登录'
          break
        case 408:
          error.message = '请求超时'
          break
        case 410:
          error.message = '需要重新刷新 Token 凭证！'
          break
        case 500:
          error.message = '服务器异常，请稍候再试'
          break
        case 501:
          error.message = '网络未实现'
          break
        case 502:
          error.message = '网络错误'
          break
        case 503:
          error.message = '服务不可用'
          break
        case 504:
          error.message = '网络超时'
          break
        case 505:
          error.message = 'http版本不支持该请求'
          break
        case 516:
          error.message = '非法参数'
          break
        default:
          error.message = `连接错误${error.response.status}`
      }
    } else {
      error.message = '连接到服务器失败'
    }

    if (error.response.status === 412) {
      notifyWarning('由于您长时间未操作，已为您刷新 Token 凭证！\n 即将为您刷新页面！')
      // 刷新token凭证
      setTimeout(() => {
        refreshToken().then(res => {
          const { data } = res.data
          if (data && res.status === 200) {
            setToken(data['token'])
            setRefreshToken(data['refreshToken'])
            if (!data['avatar']) {
              // data.user.avatar = 'https://all-imgs.muycode.com/heart-1.jpg'
              // data['avatar'] = 'https://life.muycode.com/DSC_1566.jpg'
              data['avatar'] = 'https://all-imgs.muycode.com/m-auth.jpg'
            }
            setUsername(data['loginName'])
            // orgName
            setOrgName(data['orgName'])
            setAccess(data['mids'])
            data['mids'] = ''
            setUser(JSON.stringify(data))
            setTimeout(function() {
              router.go(0)
            }, 100)
          } else {
            tokenClear()
            router.push('/login')
            errorMessage('登录失败')
          }
        })
      }, 100)
    } else {
      if (error.response.status !== 515) {
        errorMessage(error.message)
      }
    }
    return Promise.resolve(error.response)
  }
)

export default service
