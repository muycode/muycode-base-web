import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    meta: { access: 1 },
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    meta: { access: 1 },
    hidden: true
  },
  {
    path: '/system-maintenance',
    component: () => import('@/views/system-maintenance.vue'),
    meta: { access: 1 },
    hidden: true
  }
  // 动态路由，无需创建菜单路由
  // 404 page must be placed at the end !!!
  /* {
    path: '*',
    redirect: '/404',
    hidden: true
  } */
]

const createRouter = () => new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
