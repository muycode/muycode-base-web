export function encrypt(data) {
  return 'm' + window.btoa(data) + 'y'
}

export function decode(data) {
  return window.atob(data.substring(1, data.length - 1))
}
