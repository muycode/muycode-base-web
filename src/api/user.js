import request from '@/utils/request'

export function login(data) {
  return request({
    url: 'unauth/login',
    method: 'post',
    data
  })
}

export function logout() {
  const data = { loginName: localStorage.getItem('username') }
  return request({
    url: 'unauth/logout',
    method: 'post',
    data
  })
}

export function buildMenu() {
  const data = {}
  return request({
    url: 'systemMenu/buildMenu',
    method: 'post',
    data
  })
}

export function refreshToken() {
  const data = { loginName: localStorage.getItem('username'), refreshToken: localStorage.getItem('refresh_token') }
  return request({
    url: 'unauth/refreshToken',
    method: 'post',
    data
  })
}
