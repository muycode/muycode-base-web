import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const TodoDetailAction = {}

TodoDetailAction.queryTodoDetail = function(vm) {
  ajax.post('todoDetail/queryTodoDetail', { todoDetailsTimes: vm.searchTimeArray },
    res => {
      if (res.code === 200) {
        vm.todoItemList = res.data
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

TodoDetailAction.saveAndFlush = function(vm) {
  ajax.post('todoDetail/saveAndFlush', vm.dataForm,
    res => {
      if (res.code === 200) {
        successMessage('待办已添加')
        vm.$emit('closeAddForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

TodoDetailAction.changeTodo = function(vm, rid, type) {
  ajax.post('todoDetail/changeTodo', { rid: rid, type: type },
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refreshC(vm.dateYearmonth, vm.dateData)
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}
export default TodoDetailAction
