import ajax from '@/utils/ajaxApi'
import settings from '@/settings'
import { errorMessage } from '@/utils/alertMessage'

const homeAction = {}

// 查询首页数据
homeAction.getNoticeTotal = function(vm) {
  ajax.post('systemNotice/getNoticeTotal', {},
    res => {
      if (res.code === 200) {
        settings.noticeTotal = Number(res.data)
        vm.listLoading = false
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default homeAction
