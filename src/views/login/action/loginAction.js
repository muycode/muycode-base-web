import ajax from '@/utils/ajaxApi'
import Vue from 'vue'
import { setRefreshToken, setToken, setUsername } from '@/utils/auth'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const homeAction = {}

homeAction.login = function(vm) {
  ajax.post('api/auth/login', vm.loginForm,
    res => {
      if (res && res.code === 200) {
        // 请求成功
        const data = res.data
        let msg = ''
        if (data && data.user && data.status === 200) {
          setToken(data.token)
          setRefreshToken(data.refreshToken)
          setUsername(data.user.loginName)
          successMessage('登录成功')
          Vue.prototype.$router.push({ path: this.redirect || '/home' })
        } else {
          switch (data.status) {
            case 4001:
            case 4004:
              msg = '用户名或密码不正确'
              break
            case 4002:
              msg = '用户已被锁定,请联系管理员'
              break
            case 4003:
              msg = '无效用户,请联系管理员'
              break
            case 4005:
              msg = '账户已过期,请联系管理员'
              break
            case 5001:
              msg = '授权失败,请重试'
              break
          }
          if (msg === '') {
            msg = data.message
          }
          vm.errorMsgFlag = true
          vm.errorMsg = msg
        }
      }
    },
    res => {
      errorMessage(res.message)
    })
}

homeAction.getVerifyCode = function(vm) {
  ajax.get('unauth/captcha/getVerifyCode', {},
    res => {
      if (res.code === 200) {
        vm.loginForm.key = res.data.key
        vm.verifyCode = res.data.code.replaceAll('muycode', '')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default homeAction
