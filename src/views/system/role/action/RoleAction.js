import ajax from '@/utils/ajaxApi'
import { successMessage, errorMessage } from '@/utils/alertMessage'

const roleAction = {}

roleAction.queryAllList = function(vm) {
  ajax.get('systemRole/queryAllList', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data.list
        vm.tableData.total = res.data.total
        vm.tableData.size = res.data.pageSize
        vm.listLoading = false
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

roleAction.saveAndFlush = function(vm) {
  ajax.post('systemRole/saveAndFlush', vm.dataForm,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeRoleForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

roleAction.removeRole = function(vm) {
  ajax.post('systemRole/removeRole', { rid: vm.selectId },
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refreshList()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

// 查询全部的角色 -- 授权角色
roleAction.queryAllListByRole = function(vm) {
  ajax.get('systemRole/queryAllListTree', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.disposeTreeData(res.data)
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

// 查询用户所拥有的角色 -- 授权角色
roleAction.queryRoleByUser = function(vm, userId) {
  ajax.post('systemRole/getRoleListByUserId', { userId: userId },
    res => {
      if (res.code === 200) {
        const data = res.data
        const arr = []
        data.forEach(item => {
          arr.push(item['roleId'])
        })
        vm.selectRoles = arr
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

roleAction.addUserRole = function(vm) {
  ajax.post('systemRole/addUserRole', { userId: vm.selectId, roleIdStrings: vm.selectRolesFinal.join(',') },
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeUserRole')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

// 查询所有的菜单 -- 授权菜单
roleAction.queryMenuAllList = function(vm) {
  ajax.get('systemMenu/queryListByAuth', {},
    res => {
      if (res.code === 200) {
        vm.disposeTreeData(res.data)
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

// 查询用户所拥有的菜单 -- 授权菜单
roleAction.queryMenuListByUser = function(vm, roleId) {
  ajax.post('systemMenu/queryMenuByRole', { roleId: roleId },
    res => {
      if (res.code === 200) {
        vm.selectMenus = res.data
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

// 角色管理 -- 授权菜单
roleAction.addRolePermission = function(vm) {
  ajax.post('systemMenu/addRoleMenu', { roleId: vm.selectId, menuIds: vm.selectMenusFinal.join(',') },
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeRoleMenu')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default roleAction
