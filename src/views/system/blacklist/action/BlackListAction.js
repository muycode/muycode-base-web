import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const blackListAction = {}

blackListAction.queryAllList = function(vm) {
  ajax.get('blackList/queryAllList', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data.list
        vm.tableData.total = res.data.total
        vm.tableData.page = res.data.pageNum
        vm.tableData.size = res.data.pageSize
        vm.listLoading = false
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      errorMessage(res.message)
    }
  )
}

blackListAction.saveAndFlush = function(vm) {
  ajax.post('blackList/saveAndFlush', vm.dataForm,
    res => {
      if (res.success && res.code === 200) {
        successMessage(res.message)
        vm.$emit('closeBlackListForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

blackListAction.changeStatus = function(vm) {
  ajax.post('blackList/changeStatus', vm.changeParams,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refreshList()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default blackListAction
