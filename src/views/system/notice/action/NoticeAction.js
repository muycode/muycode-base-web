import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const noticeAction = {}

noticeAction.queryAllList = function(vm) {
  ajax.get('systemNotice/queryAllList', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data.list
        vm.tableData.total = res.data.total
        vm.tableData.page = res.data.pageNum
        vm.tableData.size = res.data.pageSize
        vm.listLoading = false
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      vm.listLoading = false
      errorMessage(res.message)
    }
  )
}

noticeAction.saveAndFlush = function(vm) {
  ajax.post('systemNotice/saveAndFlush', vm.dataForm,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

noticeAction.changeStatus = function(vm) {
  ajax.post('systemNotice/changeStatus', vm.changeParams,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refreshList()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

noticeAction.getOrgByUser = function(vm) {
  ajax.get('systemOrg/queryOrgListAuth', {},
    res => {
      if (res.code === 200) {
        const json = JSON.stringify(res.data).replaceAll(',"children":[]', '').replaceAll('"id"', '"value"')
        vm.orgOptions = JSON.parse(json)
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default noticeAction
