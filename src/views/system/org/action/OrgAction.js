import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const orgAction = {}

orgAction.queryAllList = function(vm) {
  ajax.get('systemOrg/queryOrgList', {},
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      vm.listLoading = false
      errorMessage(res.message)
    }
  )
}

orgAction.saveAndFlush = function(vm) {
  ajax.post('systemOrg/saveAndFlush', vm.dataForm,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeOrgForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

orgAction.delete = function(vm, data) {
  ajax.post('systemOrg/delete', data,
    res => {
      if (res.code === 200) {
        successMessage()
      } else {
        errorMessage(res.message)
      }
      vm.refreshList()
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default orgAction
