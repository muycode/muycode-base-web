import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const userAction = {}

userAction.queryAllList = function(vm) {
  ajax.get('systemUser/queryAllList', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data.list
        vm.tableData.total = res.data.total
        vm.tableData.page = res.data.pageNum
        vm.tableData.size = res.data.pageSize
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      errorMessage(res.message)
      vm.listLoading = false
    }
  )
}

userAction.changeStatus = function(vm) {
  ajax.post('systemUser/changeStatus', vm.changeParams,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refresh()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

userAction.uploadAvatar = function(vm) {
  ajax.post('systemUser/uploadAvatar', { imageBase: vm.uploadImageBase64 },
    res => {
      if (res.code === 200) {
        const user = JSON.parse(localStorage.getItem('user'))
        const userNew = res.data
        user['avatar'] = userNew.avatar
        localStorage.setItem('user', JSON.stringify(user))
        vm.initForm()
        vm.uploadAvatarBox = false
        vm.$router.go(0)
        successMessage('头像已更新，正在重新刷新页面')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

userAction.updatePwd = function(vm) {
  ajax.post('systemUser/updatePwd', vm.resetPasswordForm,
    res => {
      if (res.code === 200) {
        successMessage(res.message)
        vm.resetPasswordShow = false
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

userAction.updateUserInfo = function(vm) {
  ajax.post('systemUser/updateUserInfo', vm.updateUserInfoData,
    res => {
      if (res.code === 200) {
        const user = JSON.parse(localStorage.getItem('user'))
        const userNew = res.data
        user['email'] = userNew.email
        user['phone'] = userNew.phone
        user['sex'] = userNew.sex
        localStorage.setItem('user', JSON.stringify(user))
        successMessage('信息已更新')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

userAction.resetPwd = function(vm) {
  ajax.post('systemUser/resetPwd', { rid: vm.selectId },
    res => {
      if (res.code === 200) {
        successMessage(res.message)
        vm.refresh()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

userAction.saveAndFlush = function(vm) {
  ajax.post('systemUser/saveAndFlush', vm.dataForm,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeUserForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

// 授权组织架构

// 查询该用户的组织机构下的组织机构 -- 授权组织机构
userAction.queryAllListByOrg = function(vm) {
  ajax.get('systemOrg/queryOrgListAuth', {},
    res => {
      if (res.code === 200) {
        vm.disposeTreeData(res.data)
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

// 查询用户所拥有的菜单 -- 授权组织机构
userAction.getOrgListByUser = function(vm, userId) {
  ajax.post('systemOrg/getOrgListByUser', { userId: userId },
    res => {
      if (res.code === 200) {
        vm.selectOrgs = res.data
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

// 角色管理 -- 授权组织架构
userAction.addUserOrg = function(vm) {
  ajax.post('systemOrg/addUserOrg', { userId: vm.selectId, orgIds: vm.selectOrgsFinal.join(',') },
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeUserOrg')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

userAction.updateOpenSms = function(vm) {
  ajax.post('systemUser/updateOpenSms', vm.openSmsData,
    res => {
      if (res.code === 200) {
        successMessage(res.message)
        vm.openSmsData = {}
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default userAction
