import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const dictActionData = {}

dictActionData.queryAllList = function(vm) {
  ajax.get('systemDictData/queryAllList', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data.list
        vm.tableData.total = res.data.total
        vm.tableData.page = res.data.pageNum
        vm.tableData.size = res.data.pageSize
        vm.listLoading = false
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      vm.listLoading = false
      errorMessage(res.message)
    }
  )
}

dictActionData.saveAndFlush = function(vm) {
  ajax.post('systemDictData/saveAndFlush', vm.dataForm,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictActionData.changeStatus = function(vm) {
  ajax.post('systemDictData/changeStatus', vm.changeParams,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refreshList()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default dictActionData
