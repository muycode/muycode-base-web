import ajax from '@/utils/ajaxApi'
import { errorMessage } from '@/utils/alertMessage'

const dictBaseAction = {}

// 获取字典类型类型
dictBaseAction.getDictByCode = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.getDictByCode2 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode2 },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues2 = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.getDictByCode3 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode3 },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues3 = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.getDictByCode4 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode4 },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues4 = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.getDictByCode5 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode5 },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues5 = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.getDictByCode6 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode6 },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues6 = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.getDictByCode7 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode7 },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues7 = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.getDictByCode8 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode8 },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues8 = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.getDictByCode9 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode9 },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues9 = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.getDictByCode10 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode10 },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.dictValues10 = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.dictListAll = function(vm) {
  ajax.get('systemDict/getDictListAll', {},
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data.forEach((item) => {
          array.push({ label: item['title'], value: item['code'] })
        })
        vm.dictListAll = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

dictBaseAction.dictListAllById = function(vm) {
  ajax.get('systemDict/getDictListAll', {},
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data.forEach((item) => {
          array.push({ label: item['title'], value: item['rid'] })
        })
        vm.dictListAll = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default dictBaseAction
