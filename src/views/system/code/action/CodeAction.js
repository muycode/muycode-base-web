import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const CodeAction = {}

CodeAction.queryAllList = function(vm) {
  ajax.get('systemCodeGenerate/queryAllList', vm.searchForm, res => {
    if (res.code === 200) {
      vm.tableData.rows = res.data.list
      vm.tableData.total = res.data.total
      vm.tableData.page = res.data.pageNum
      vm.tableData.size = res.data.pageSize
    } else {
      errorMessage(res.message)
    }
    vm.listLoading = false
  }, res => {
    errorMessage(res.message)
    vm.listLoading = false
  })
}

CodeAction.getTablesByDataBase = function(vm) {
  ajax.get('systemCodeGenerate/getTablesByDataBase', {}, res => {
    if (res.code === 200) {
      vm.tableNames = res.data
    } else {
      errorMessage(res.message)
    }
  }, res => {
    errorMessage(res.message)
  })
}

CodeAction.saveAndFlush = function(vm) {
  ajax.post('systemCodeGenerate/saveAndFlush', vm.dataForm, res => {
    if (res.success && res.code === 200) {
      successMessage()
      vm.$emit('closeForm')
    } else {
      errorMessage(res.message)
    }
  }, res => {
    errorMessage(res.message)
  })
}

CodeAction.delete = function(vm) {
  ajax.post('systemCodeGenerate/delete', vm.changeParams, res => {
    if (res.success && res.code === 200) {
      successMessage()
      vm.refreshList()
    } else {
      errorMessage(res.message)
    }
  }, res => {
    errorMessage(res.message)
  })
}

CodeAction.listByGenId = function(vm) {
  ajax.post('systemCodeGenerateInfo/listByGenId', vm.searchParams, res => {
    if (res.success && res.code === 200) {
      vm.fieldList = res.data
      vm.fieldList.forEach((item) => {
        if (item['formType'] === '3' || item['formType'] === '4' || item['formType'] === '8' || item['formType'] === '9') {
          item['isSelectDirct'] = true
        } else {
          item['isSelectDirct'] = false
        }
        if (item['whetherQuery'] === '1') {
          item['isWhetherQuery'] = true
        } else {
          item['isWhetherQuery'] = false
        }
      })
    } else {
      errorMessage(res.message)
    }
    vm.listLoading = false
  }, res => {
    vm.listLoading = false
    errorMessage(res.message)
  })
}

CodeAction.getDictByCode4 = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCode4 }, res => {
    if (res.code === 200) {
      const data = res.data
      const array = []
      data['systemDictDataList'].forEach((item) => {
        array.push({ label: item['code'], value: item['value'] })
      })
      vm.dictValues4 = array
    } else {
      errorMessage(res.message)
    }
  }, res => {
    errorMessage(res.message)
  })
}

CodeAction.saveAndFlushInfo = function(vm) {
  ajax.post('systemCodeGenerateInfo/saveAndFlushInfo', vm.fieldList, res => {
    if (res.code === 200) {
      successMessage()
      vm.$emit('closeSettingForm')
    } else {
      errorMessage(res.message)
    }
  }, res => {
    errorMessage(res.message)
  })
}

CodeAction.generateCodeByTable = function(vm) {
  ajax.post('systemCodeGenerate/generateCodeByTable', vm.changeParams, res => {
    if (res.code === 200) {
      successMessage()
      if (res.data && res.data.indexOf('http') > -1) {
        location.href = res.data
      }
      vm.refreshList()
    } else {
      errorMessage(res.message)
    }
    vm.listLoading = false
  }, res => {
    vm.listLoading = false
    errorMessage(res.message)
  })
}

CodeAction.getLocalProjectPath = function(vm) {
  ajax.get('systemCodeGenerate/getLocalProjectPath', {}, res => {
    if (res.code === 200) {
      vm.localJavaPath = res.data
    } else {
      errorMessage(res.message)
    }
    vm.listLoading = false
  }, res => {
    vm.listLoading = false
    errorMessage(res.message)
  })
}

CodeAction.resetGenerateInfo = function(vm, genId) {
  ajax.post('systemCodeGenerate/resetGenerateInfo', vm.searchParams, res => {
    if (res.code === 200) {
      vm.refreshField(genId)
    } else {
      errorMessage(res.message)
    }
    vm.fullscreenLoading = false
  }, res => {
    vm.listLoading = false
    errorMessage(res.message)
  })
}

// 选取父级菜单
CodeAction.getMenuByGenerate = function(vm) {
  ajax.get('systemMenu/getMenuByGenerate', {}, res => {
    if (res.code === 200) {
      const json = JSON.stringify(res.data).replaceAll(',"children":[]', '').replaceAll('title', 'label')
      vm.menuTrees = JSON.parse(json)
    } else {
      errorMessage(res.message)
    }
  }, res => {
    errorMessage(res.message)
  })
}

export default CodeAction
