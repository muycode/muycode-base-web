import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const menuAction = {}

menuAction.queryAllList = function(vm) {
  ajax.get('systemMenu/queryAllList', {},
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      vm.listLoading = false
      errorMessage(res.message)
    }
  )
}

menuAction.saveAndFlush = function(vm) {
  ajax.post('systemMenu/saveAndFlush', vm.dataForm,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeMenuForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

menuAction.delete = function(vm, data) {
  ajax.post('systemMenu/delete', data,
    res => {
      if (res.code === 200) {
        successMessage()
      } else {
        errorMessage(res.message)
      }
      vm.refreshList()
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default menuAction
