import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const fileAction = {}

fileAction.queryAllList = function(vm) {
  ajax.get('systemFile/queryAllList', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data.list
        vm.tableData.total = res.data.total
        vm.tableData.page = res.data.pageNum
        vm.tableData.size = res.data.pageSize
        vm.listLoading = false
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      vm.listLoading = false
      errorMessage(res.message)
    }
  )
}

// 获取字典类型类型
fileAction.getDictByCode = function(vm) {
  ajax.get('systemDict/getDictByCode', { code: vm.dictCodeBucket },
    res => {
      if (res.code === 200) {
        const data = res.data
        const array = []
        data['systemDictDataList'].forEach((item) => {
          array.push({ label: item['code'], value: item['value'] })
        })
        vm.bucketList = array
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

fileAction.deleteFile = function(vm, row) {
  ajax.post('systemFile/deleteFile', row,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refreshList()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

fileAction.deleteFileByFileName = function(vm, data) {
  ajax.post('systemFile/deleteFileByFileName', { fileName: data },
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refreshList()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default fileAction
