import ajax from '@/utils/ajaxApi'
import { successMessage, errorMessage } from '@/utils/alertMessage'

const taskAction = {}

taskAction.queryAllList = function(vm) {
  ajax.get('systemTask/queryAllList', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data.list
        vm.tableData.total = res.data.total
        vm.tableData.page = res.data.pageNum
        vm.tableData.size = res.data.pageSize
        vm.listLoading = false
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      vm.listLoading = false
      errorMessage(res.message)
    }
  )
}

taskAction.saveAndFlush = function(vm) {
  ajax.post('systemTask/saveAndFlush', vm.dataForm,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

taskAction.changeStatus = function(vm) {
  ajax.post('systemTask/changeStatus', vm.changeParams,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refreshList()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

taskAction.getTaskStatus = function(vm) {
  ajax.get('systemTask/getTaskStatus', vm.changeParams,
    res => {
      if (res.code === 200) {
        vm.taskStatus = JSON.parse(res.data).message
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

taskAction.getTaskRecord = function(vm) {
  ajax.get('systemTask/getTaskRecord', vm.changeParams,
    res => {
      if (res.code === 200) {
        vm.tableData = res.data
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default taskAction
