import ajax from '@/utils/ajaxApi'
import { errorMessage, successMessage } from '@/utils/alertMessage'

const configAction = {}

configAction.queryAllList = function(vm) {
  ajax.get('systemConfig/queryAllList', vm.searchForm,
    res => {
      if (res.code === 200) {
        vm.tableData.rows = res.data.list
        vm.tableData.total = res.data.total
        vm.tableData.page = res.data.pageNum
        vm.tableData.size = res.data.pageSize
        vm.listLoading = false
      } else {
        errorMessage(res.message)
      }
      vm.listLoading = false
    },
    res => {
      vm.listLoading = false
      errorMessage(res.message)
    }
  )
}

configAction.saveAndFlush = function(vm) {
  ajax.post('systemConfig/saveAndFlush', vm.dataForm,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.$emit('closeForm')
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

configAction.changeStatus = function(vm) {
  ajax.post('systemConfig/changeStatus', vm.changeParams,
    res => {
      if (res.code === 200) {
        successMessage()
        vm.refreshList()
      } else {
        errorMessage(res.message)
      }
    },
    res => {
      errorMessage(res.message)
    }
  )
}

export default configAction
